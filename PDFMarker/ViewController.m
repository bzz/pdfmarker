//
//  ViewController.m
//  PDFMarker
//
//  Created by Mikhail Baynov on 10/23/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
}
@property (nonatomic, strong) NSMutableArray *modelArray;

@property UILongPressGestureRecognizer * longPressRecognizer;
@property UILongPressGestureRecognizer * clearPageRecognizer;
@property float pageHeightCommon;
@property float pageWidthCommon;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.contentViewController = [[ContentViewController alloc]init];
	
}

- (void)viewDidAppear:(BOOL)animated {
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"hardcover"]];
	self.pageNumber = 1;
	self.noDeleteFileAfterUse = YES;
	if (!self.fileUrl) {
		MenuViewController *mvc = [[MenuViewController alloc]init];
		[self presentViewController:mvc animated:YES completion:nil];
	} else {
		self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation: UIPageViewControllerNavigationOrientationHorizontal options:nil];
		self.pageViewController.delegate = self;
		self.pageViewController.dataSource = self;
		

		
//		[self addChildViewController:self.pageViewController];
//		[self.pageViewController didMoveToParentViewController:self];
//		self.pageViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		
		[self getPDFDocumentAtURL:self.fileUrl];
		

		
		self.modelArray = [[NSMutableArray alloc] init];
		[self.modelArray addObject:[self contentViewControllerForPage:1 ofPDFDocument:self.PDFDocument]];   /// page No.0

		self.pageHeightCommon = 0;
		self.pageWidthCommon = 0;
		for (int index = 1; index <= self.pageMaxNumber; index++) {
			ContentViewController * cvc = [self contentViewControllerForPage:index ofPDFDocument:self.PDFDocument];
			if (self.pageHeightCommon < cvc.cropBox.size.height) {
				self.pageHeightCommon = cvc.cropBox.size.height;
			}
			if (self.pageWidthCommon < cvc.cropBox.size.width) {
				self.pageWidthCommon = cvc.cropBox.size.width;
			}
			[self.modelArray addObject:cvc];			
		}
		
//		NSLog(@"selfview %f x %f",self.view.bounds.size.width, self.view.bounds.size.height);

		self.pageViewController.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
//		NSLog(@"bounds %f x %f",self.pageViewController.view.bounds.size.width, self.pageViewController.view.bounds.size.height);

		
		self.pageNumber = 1;
		self.contentViewController = self.modelArray[self.pageNumber];
		[self.pageViewController setViewControllers:@[self.contentViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
		
		[self.view addSubview:self.pageViewController.view];
		

		
		


		
		[self addGestureRecognisers];
		for (UIGestureRecognizer *gR in self.pageViewController.gestureRecognizers) {
			gR.delegate = self;
		}
		
	}
}

- (void)getPDFDocumentAtURL:(NSURL*)pdfURL {
	self.PDFDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)pdfURL);
	self.pageMaxNumber = (int)CGPDFDocumentGetNumberOfPages(self.PDFDocument);
	NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[pdfURL path] error:nil];
	self.PDFDocumentFileName = [pdfURL lastPathComponent];
	NSDate *pdfDate = [attributes fileModificationDate];
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"yyyy-MMdd-HHmm"];
	self.PDFDocumentDate = [dateFormat stringFromDate:pdfDate];
	if (!self.noDeleteFileAfterUse) {
		[self removeItemAtURL:pdfURL];
	}
}

- (ContentViewController *)contentViewControllerForPage:(int)page ofPDFDocument:(CGPDFDocumentRef)document {
	ContentViewController *cvc;
	cvc = [[ContentViewController alloc] initWithPDFPage:CGPDFDocumentGetPage(document, page) markupKey:[NSMutableString stringWithFormat:@"%@(%@)pg%i",self.PDFDocumentFileName, self.PDFDocumentDate, page] frame:self.pageViewController.view.frame];

	return cvc;
}

- (void) removeItemAtURL:(NSURL *)pdfURL {
	NSError *removeError;
	bool result = [[NSFileManager defaultManager] removeItemAtURL:pdfURL error:&removeError];
	if (!result)
	{ NSLog(@"ERROR REMOVING ITEM: %@",removeError);}
	else {NSLog(@"ITEM REMOVED");}
}



	
	
	
	/*
	 thePageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation: UIPageViewControllerNavigationOrientationHorizontal options:nil];
	 
	 thePageViewController.delegate = self;
	 thePageViewController.dataSource = self;
	 
	 thePageViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	 
	 contentViewController = [[ContentViewController alloc] initWithPDF:PDFDocument];
	 contentViewController.page = [modelArray objectAtIndex:0];
	 */

	






#pragma mark - UIPageViewControllerDataSource Methods

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSUInteger index = [self indexOfViewController:(ContentViewController*)viewController];
    if (index < 2 || index == NSNotFound) {
        return self.modelArray[1];
    }
    index--;
	self.pageNumber = index;
	NSLog(@"%i",self.pageNumber);
	self.contentViewController = self.modelArray[index];
	return  self.contentViewController;
	
/*
	if (self.pageNumber > 1) {
		self.pageNumber--;
		self.contentViewController = self.modelArray[self.pageNumber];
		NSLog(@"%i",self.pageNumber);
		return self.contentViewController;
	} else {
		return self.modelArray[1];
	}
*/
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
	{
		NSUInteger index = [self indexOfViewController:(ContentViewController*)viewController];
		if (index >= self.pageMaxNumber) {
			NSLog(@"%dindex",index);
			return self.modelArray[self.pageMaxNumber];
		}
		index++;
		self.pageNumber = index;
		NSLog(@"%i",self.pageNumber);
		self.contentViewController = self.modelArray[index];
		return  self.contentViewController;
	}

/*
	
	
	if (self.pageNumber < self.pageMaxNumber) {
		self.pageNumber++;
		self.contentViewController = self.modelArray[self.pageNumber];
		NSLog(@"%i",self.pageNumber);
		return self.contentViewController;
	} else {
		return self.modelArray[self.pageMaxNumber];
	}
*/
}


- (NSUInteger) indexOfViewController:(ContentViewController *) viewController
{
    return [self.modelArray indexOfObject:viewController];
}




#pragma mark - UIPageViewControllerDelegate Methods

- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController
                   spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation {
	return UIPageViewControllerSpineLocationMin;
}

#pragma mark - Gesture recgnzrs

- (void)addGestureRecognisers {
	self.longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
	self.longPressRecognizer.minimumPressDuration = 0.4;
	[self.view addGestureRecognizer:self.longPressRecognizer];
	
	self.clearPageRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(clearPage:)];
	self.longPressRecognizer.minimumPressDuration = 0.4;
	[self.clearPageRecognizer setNumberOfTouchesRequired:5];
	[self.view addGestureRecognizer:self.clearPageRecognizer];
	
	[self.clearPageRecognizer requireGestureRecognizerToFail:self.longPressRecognizer];
}

- (void)removeGestureRecognisers {
	for (UIGestureRecognizer *recognizer in self.view.gestureRecognizers) {
		[self.view removeGestureRecognizer:recognizer];
	}
}

- (void)clearPage:(UISwipeGestureRecognizer *)recogniser {
	[[NSUserDefaults standardUserDefaults] setObject:nil forKey:self.contentViewController.markupKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
	[self.contentViewController.markupView setNeedsDisplay];
}


- (void)longPress:(UILongPressGestureRecognizer *)recognizer {
	self.contentViewController.markupView.editMode = YES;
	[self removeGestureRecognisers];
	[self addButtons];

	//	[self.contentViewController.markupView becomeFirstResponder];
	
}

#pragma mark - Buttons


- (void)addButtons {
	if (!self.editButton) {
		self.editButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[self.editButton addTarget:self
							action:@selector(editModeOff)
				  forControlEvents:UIControlEventTouchDown];
		[self.editButton setTitle:@"o" forState:UIControlStateNormal];
		self.editButton.frame = CGRectMake(self.view.bounds.size.width - 200, 10.0f, 60.0f, 60.0f);
		self.editButton.backgroundColor = [UIColor redColor];
		self.editButton.layer.cornerRadius = 10;
		[self.view addSubview:self.editButton];
	}
	
	if (!self.menuButton) {
		self.menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[self.menuButton addTarget:self
							action:@selector(toMainMenu)
				  forControlEvents:UIControlEventTouchDown];
		[self.menuButton setTitle:@"M" forState:UIControlStateNormal];
		self.menuButton.frame = CGRectMake(self.view.bounds.size.width - 130, 10.0f, 60.0f, 60.0f);
		self.menuButton.backgroundColor = [UIColor greenColor];
		self.menuButton.layer.cornerRadius = 10;
		[self.view addSubview:self.menuButton];
	}
	
}


- (void)removeButtons {
	[self.editButton removeFromSuperview];
	[self.menuButton removeFromSuperview];
	self.editButton = nil;
	self.menuButton = nil;
}

- (void)editModeOff {
	NSLog(@"EDIT PRESSED");
	[self removeButtons];
	self.contentViewController.markupView.editMode = NO;
	[self addGestureRecognisers];
}

- (void)toMainMenu {
	NSLog(@"MENU PRESSED");
	[self removeButtons];
	MenuViewController *mvc = [[MenuViewController alloc]init];
	[self presentViewController:mvc animated:YES completion:nil];
}

 
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//Touch gestures below top bar should not make the page turn.
//EDITED Check for only Tap here instead.
//    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
//		CGPoint touchPoint = [touch locationInView:self.view];
//        if (touchPoint.y > 40) {

    if (self.contentViewController.markupView.editMode == YES) {
		return NO;
	} else {
	return YES;
	}
}




#pragma mark - Rotation handling

- (BOOL)shouldAutorotate {
		return NO;
}



- (void)dealloc {
	CGPDFDocumentRelease(self.PDFDocument);
}


@end
