//
//  ContentViewController.h
//  PDFMarker
//
//  Created by Mikhail Baynov on 11/2/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import "ViewController.h"
#import "MarkupView.h"
#import "TiledPDFView.h"

@interface ContentViewController : UIViewController <PaintViewDelegate>


@property (nonatomic, strong) TiledPDFView* pdfView;
@property (nonatomic, strong) MarkupView* markupView;

@property (nonatomic, strong) NSString* PDFDocumentFileName;
@property (nonatomic, strong) NSString* PDFDocumentDate;
@property (nonatomic, strong) NSMutableString* markupKey;
@property (nonatomic, strong) NSMutableArray *markupArray;
@property CGPDFPageRef PDFPage;
@property (nonatomic) CGRect cropBox;
@property (nonatomic) CGRect cropBoxRaw;
@property (nonatomic) float scale;



- (id)initWithPDFPage:(CGPDFPageRef)page markupKey:(NSMutableString *)markupKey frame:(CGRect)frame;



@end
