//
//  ViewController.h
//  PDFMarker
//
//  Created by Mikhail Baynov on 10/23/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "MenuViewController.h"
#import "ContentViewController.h"
@class MenuViewController, ContentViewController;

@interface ViewController : UIViewController <UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIGestureRecognizerDelegate>
@property (nonatomic, strong) NSURL* fileUrl;
@property int pageNumber;
@property int pageMaxNumber;
@property (nonatomic) BOOL noDeleteFileAfterUse;
@property (nonatomic) CGPDFDocumentRef PDFDocument;
@property (nonatomic, strong) NSString* PDFDocumentFileName;
@property (nonatomic, strong) NSString* PDFDocumentDate;

@property UIPageViewController * pageViewController;
@property (nonatomic, strong) ContentViewController * contentViewController;
@property (nonatomic, strong) UIButton* editButton;
@property (nonatomic, strong) UIButton* menuButton;

@end
