//
//  ContentViewController.m
//  PDFMarker
//
//  Created by Mikhail Baynov on 11/2/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import "ContentViewController.h"
#import "MenuViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface ContentViewController ()
@property UIImageView * lowResolutionImage;
@property (nonatomic) float offsetLeft;


@end

@implementation ContentViewController



- (id)initWithPDFPage:(CGPDFPageRef)page markupKey:(NSMutableString *)markupKey frame:(CGRect)frame{
	self = [super init];
	if (self) {
		self.PDFPage = page;
		self.cropBoxRaw = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
		
		float a1;
		float a2;
		if (self.cropBoxRaw.size.width > self.cropBoxRaw.size.height) {
			a1 = frame.size.width / self.cropBoxRaw.size.height;
			a2 = frame.size.height / self.cropBoxRaw.size.width;
			if (a1 > a2) {
				a1 = a2;
			}
		} else {
			a1 = frame.size.width / self.cropBoxRaw.size.width;
			a2 = frame.size.height / self.cropBoxRaw.size.height;
			if (a1 > a2) {
				a1 = a2;
			}
		}
		self.scale = a1;
		self.cropBox = CGRectMake(0, 0, self.cropBoxRaw.size.width*a1, self.cropBoxRaw.size.height*a1);
		NSLog(@"cropboxRaw %f x %f",self.cropBoxRaw.size.width, self.cropBoxRaw.size.height);
		self.markupKey = markupKey;
	}
	return self;
}

- (void)viewDidAppear:(BOOL)animated {

}


- (void)viewDidLoad
{
    [super viewDidLoad];
	[self showPage];
	[self showMarkup];
}




- (void) showPage {
		self.markupView = nil;
		[self.pdfView removeFromSuperview];
		self.pdfView = nil;
		
		self.pdfView = [[TiledPDFView alloc] initWithFrame:self.view.bounds scale:self.scale];
		self.offsetLeft = self.view.bounds.size.width - self.cropBox.size.width;
		[self drawPageInLowResolution:self.PDFPage withScale:self.scale];
		[self.pdfView setPage:self.PDFPage];
		self.PDFPage = nil;
		[self.view addSubview:self.pdfView];
}



- (void) showMarkup {
	NSLog(@"%@",self.markupKey);
	self.markupView = [[MarkupView alloc] initWithMarkupKey:self.markupKey frame:self.pdfView.bounds];
	self.markupView.alpha = .2;
	self.markupView.backgroundColor = [UIColor clearColor];
	self.markupView.delegate = self;
	[self.pdfView addSubview:self.markupView];
}


#pragma mark - Draw low resolution screen

- (void)drawPageInLowResolution:(CGPDFPageRef)PDFPage withScale:(float)scale;
{
    
    if (self.lowResolutionImage != nil) {
        [self.lowResolutionImage removeFromSuperview];
    }
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[self backgroundImageForPage:PDFPage withScale:scale]];
    backgroundImageView.frame = self.pdfView.bounds;
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.pdfView addSubview:backgroundImageView];
    [self.pdfView sendSubviewToBack:backgroundImageView];
    self.lowResolutionImage = backgroundImageView;
}


- (UIImage *)backgroundImageForPage:(CGPDFPageRef)PDFPage withScale:(float)scale {
	UIGraphicsBeginImageContext(self.pdfView.bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 1.0,1.0,1.0,1.0);
    CGContextFillRect(context, self.pdfView.bounds);
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, self.offsetLeft, self.pdfView.bounds.size.height);   // Flip the context
    CGContextScaleCTM(context, scale, -scale);											// Flip the context

	if (self.cropBoxRaw.size.width > self.cropBoxRaw.size.height) {
    CGContextTranslateCTM(context, self.cropBoxRaw.size.width, 0);
		CGContextRotateCTM(context, M_PI_2);
	}
	
	NSLog(@"cropbox width %f   height %f", self.cropBox.size.width, self.cropBox.size.height);
	NSLog(@"self.pdfView.width %f   height %f", self.pdfView.bounds.size.width, self.pdfView.bounds.size.height);
	NSLog(@"scale %f", scale);
	
    CGContextDrawPDFPage(context, PDFPage);
	CGContextRestoreGState(context);
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	return backgroundImage;
}


#pragma mark - PaintViewDelegate

- (void)addMarkupData:(NSArray*)array {
	NSLog(@"%@",self.markupKey);
	self.markupArray = [[[NSUserDefaults standardUserDefaults] objectForKey:self.markupKey] mutableCopy];
	if (!self.markupArray) {
		self.markupArray = [[NSMutableArray alloc]init];
	}
	[self.markupArray addObjectsFromArray:array];
	[[NSUserDefaults standardUserDefaults] setObject:self.markupArray forKey:self.markupKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
	///////	[[NSUserDefaults standardUserDefaults] registerDefaults:[NSDictionary dictionaryWithContentsOfFile:@"zzz.plist"]];
}






#pragma mark - Clean up
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
	//	CGPDFDocumentRelease(self.PDFDocument);
}

@end
