//
//  MenuCell.h
//  PDFMarker
//
//  Created by Mikhail Baynov on 10/31/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell


@property (nonatomic, strong) UILabel *descriptionLabel;
@end
