//
//  DrawPoint.h
//  Paint
//
//  Created by Sergey Reznik on 4/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DrawPoint : NSObject
{
	float x;
	float y;
}

+ (DrawPoint*)drawPointWithCGPoint:(CGPoint)p;
- (CGPoint)getCGPoint;

@end
