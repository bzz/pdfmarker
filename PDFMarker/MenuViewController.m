//
//  MenuViewController.m
//  PDFMarker
//
//  Created by Mikhail Baynov on 10/29/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import "MenuViewController.h"
#import "ViewController.h"
#import "ContentViewController.h"

@interface MenuViewController ()
@property float keyboardHeight;
@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"hardcover"]];
	
	[self addKeyboardNotifications];
	
	
	
	if ([[UIDevice currentDevice] orientation] > 2) {
		[self drawHorizontal];
	} else {
		[self drawVertical];
	}
	
	[self drawFileListTable];
}

- (void)addKeyboardNotifications {
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(willShowKeyboard:)
												 name:UIKeyboardWillShowNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(didShowKeyboard:)
												 name:UIKeyboardDidShowNotification
											   object:nil];
	
}

#pragma mark - Keyboard notifications

- (void)willShowKeyboard:(NSNotification *)notification {
	[UIView setAnimationsEnabled:NO];
}

- (void)didShowKeyboard:(NSNotification *)notification {
	[UIView setAnimationsEnabled:YES];
}




- (void)drawHorizontal {
	if (!self.textField) {
	self.textField = [[UITextField alloc]init];
	self.fileListTableView = [[UITableView alloc]init];
	self.webView = [[UIWebView alloc]init];
	[self.view addSubview:self.webView];
	}
	
	self.textField.frame = CGRectMake(300, 20, 400, 40);
	self.textField.backgroundColor = [UIColor greenColor];
	self.textField.delegate = self;
	[self.view addSubview:self.textField];
	
	self.fileListTableView.frame = CGRectMake(20, 20, 260, self.view.bounds.size.height - 40 - self.keyboardHeight);
	self.fileListTableView.delegate = self;
	self.fileListTableView.dataSource = self;
	[self.view addSubview:self.fileListTableView];
	
	self.webView.frame = CGRectMake(300, 80, self.view.bounds.size.width-300, self.view.bounds.size.height-80 - self.keyboardHeight);
	self.webView.backgroundColor = [UIColor redColor];
	[self.view setNeedsDisplay];
}

- (void)drawVertical {
	if (!self.textField) {
		self.textField = [[UITextField alloc]init];
		self.fileListTableView = [[UITableView alloc]init];
		self.webView = [[UIWebView alloc]init];
		[self.view addSubview:self.webView];
	}

	self.textField.frame = CGRectMake(300, 20, 400, 40);
	self.textField.backgroundColor = [UIColor greenColor];
	self.textField.delegate = self;
	[self.view addSubview:self.textField];
	[self.textField becomeFirstResponder];

	self.fileListTableView.frame = CGRectMake(20, 20, 260, self.view.bounds.size.height - 40 - self.keyboardHeight);
	self.fileListTableView.delegate = self;
	self.fileListTableView.dataSource = self;
	[self.view addSubview:self.fileListTableView];
	
	self.webView.frame = CGRectMake(300, 80, self.view.bounds.size.width-300, self.view.bounds.size.height-80 - self.keyboardHeight);
	self.webView.backgroundColor = [UIColor redColor];
	[self.view addSubview:self.webView];
}


- (void)keyboardWasShown:(NSNotification *)notification
{
	self.keyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	
	NSString *urlAddress = [NSString stringWithFormat:@"http://www.google.com/search?q=%@+filetype%%3Apdf", self.textField.text];
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];

    [textField resignFirstResponder];
    return YES;
}



-(void) drawFileListTable {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths[0] stringByAppendingString:@"/Inbox/"];
	self.fileList = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil] mutableCopy];
}


#pragma mark - TableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.fileList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
	MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[MenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
	cell.descriptionLabel.text = self.fileList[indexPath.row];
	
	return cell;
}
- (void)tableView: (UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath {
	ViewController *vc = [[ViewController alloc]init];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [NSString stringWithFormat:@"file://localhost/private%@/Inbox/%@", paths[0], self.fileList[indexPath.row]];
	NSLog(@"%@", documentsDirectory);
	vc.fileUrl = [NSURL URLWithString:documentsDirectory];
	vc.noDeleteFileAfterUse = YES;
	[self presentViewController:vc animated:NO completion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
	return 100.0f;
}


#pragma mark - Rotation handling

- (BOOL)shouldAutorotate {
	return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
