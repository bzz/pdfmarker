//
//  PaintView.h
//  Paint
//
//  Created by Sergey Reznik on 3/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrawPoint.h"

@protocol PaintViewDelegate //<UIView>

- (void)addMarkupData:(NSArray*)array;

@end


@interface MarkupView : UIView 
{
	CGPoint _pressPoint;
	CGPoint _prevPoint;
	NSMutableArray* _points;
	UIImage* _canvas;
	BOOL _drawing;
}

//@property (assign) float lineWidth;
//@property (nonatomic, retain) UIColor* paintColor;
@property 	id<PaintViewDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *backBuffer;
@property (nonatomic, strong) NSMutableString* markupKey;
@property BOOL editMode;

- (id)initWithMarkupKey:(NSString*)markupKey frame:(CGRect)frame;

@end
