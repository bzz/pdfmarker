//
//  AppDelegate.m
//  PDFMarker
//
//  Created by Mikhail Baynov on 10/18/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "ContentViewController.h"

@interface AppDelegate ()
@property ViewController *initialViewController;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"application didFinishLaunchingWithOptions");
    // Override point for customization after application launch.
	[self.window setBackgroundColor:[UIColor lightGrayColor]];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
	//    if ([[DBSession sharedSession] handleOpenURL:url]) {
	//        if ([[DBSession sharedSession] isLinked]) {
	
    NSLog(@"application handleOpenURL");

	if (!self.initialViewController.isViewLoaded) {
		NSLog(@"NO isViewLoaded");

		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		self.initialViewController = [storyboard instantiateInitialViewController];
		self.initialViewController.fileUrl = url;

		self.window.rootViewController = self.initialViewController;

	} else {
		NSLog(@"YES isViewLoaded");
		self.window.rootViewController = self.initialViewController;

		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		self.initialViewController = [storyboard instantiateInitialViewController];
		self.initialViewController.fileUrl = url;

		[self.window.rootViewController presentViewController:self.initialViewController animated:NO completion:nil];
	}
	
	NSLog(@"%@",url);

	
	
	return YES;
}

@end
