//
//  DrawPoint.m
//  Paint
//
//  Created by Sergey Reznik on 4/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DrawPoint.h"

@implementation DrawPoint

+ (DrawPoint*)drawPointWithCGPoint:(CGPoint)p
{
	DrawPoint* result = [[DrawPoint alloc] init] ;
	result->x = p.x;
	result->y = p.y;
	return result;
}

- (CGPoint)getCGPoint
{
	return CGPointMake(x, y);
}

@end