//
//  AppDelegate.h
//  PDFMarker
//
//  Created by Mikhail Baynov on 10/23/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
