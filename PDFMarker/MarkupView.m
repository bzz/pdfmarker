//
//  PaintView.m
//  Paint
//
//  Created by Sergey Reznik on 3/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#define MARKUP_LINE_WIDTH 20.0f


#import "MarkupView.h"




@interface MarkupView () {
}

@end

@implementation MarkupView

- (id)initWithMarkupKey:(NSMutableString*)markupKey frame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		self.markupKey = markupKey;
		[self setMultipleTouchEnabled:YES];
		
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
	}
	return self;
}

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
	}
	return self;
}


#pragma mark - Rendering
- (void)drawRect:(CGRect)rect
{
	[_canvas drawAtPoint:CGPointMake(0.0f, 0.0f)];
	
	[self performRenderingFromBackBufferInContext:UIGraphicsGetCurrentContext()];
 	if (_drawing) {
		[self performRenderingToContext:UIGraphicsGetCurrentContext()];
	}
}

- (void)performRenderingToContext:(CGContextRef)context {
		CGContextSetLineWidth(context, MARKUP_LINE_WIDTH);
		CGContextSetStrokeColorWithColor(context, [UIColor brownColor].CGColor);
		CGPoint p0 = [_points[0] getCGPoint];
	
		CGContextBeginPath(context);
		CGContextMoveToPoint(context, p0.x, p0.y);
	
		for (int i = 1; i < [_points count]; i++)
			{
			CGPoint pt = [_points[i] getCGPoint];
			CGContextAddLineToPoint(context, pt.x, pt.y);
			}
			CGContextStrokePath(context);
}

- (void)performRenderingFromBackBufferInContext:(CGContextRef)context {
	NSMutableArray *arrayToPass;
	arrayToPass = [[NSMutableArray alloc]init];
	arrayToPass = [[NSUserDefaults standardUserDefaults] objectForKey:self.markupKey];


//	NSLog(@"performRenderingFromBackBufferInContext arrayToPass = %@",arrayToPass);
		CGContextSetLineWidth(context, MARKUP_LINE_WIDTH);
		CGContextSetStrokeColorWithColor(context, [UIColor brownColor].CGColor);
		CGPoint p0 = CGPointFromString(arrayToPass [0]);
			if (p0.x && p0.y) {
		
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, p0.x, p0.y);
		
			CGPoint pt;
			CGPoint ptForth;
			for (int i = 1; i < [arrayToPass count]; i++)
			{
				pt = CGPointFromString(arrayToPass[i-1]);
				ptForth = CGPointFromString(arrayToPass [i]);
				CGContextAddLineToPoint(context, pt.x, pt.y);
					///if empty point in array
				if (!ptForth.x && !ptForth.y) {
					CGContextStrokePath(context);
					i++;
					if (i < [arrayToPass count]) {
						pt = CGPointFromString(arrayToPass[i-1]);
						ptForth = CGPointFromString(arrayToPass [i]);
						CGContextBeginPath(context);
						CGContextMoveToPoint(context, ptForth.x, ptForth.y);
					}
				}
			}
		CGContextStrokePath(context);
		}
	
}

- (void)performRenderingToBackBuffer {
	NSString *stringFromPoint;
	NSMutableArray *arrayToPass;
	arrayToPass = [[NSMutableArray alloc]init];
	for (int i = 0; i < [_points count]; i++) {
	stringFromPoint = NSStringFromCGPoint([_points[i] getCGPoint]);
	[arrayToPass addObject:stringFromPoint];
	}
	[arrayToPass addObject:NSStringFromCGPoint(CGPointMake(0, 0))];
	[self.delegate addMarkupData:[arrayToPass copy]];
}





#pragma mark - Touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSLog(@"%@ -- 1",self.markupKey);
	if (self.editMode == YES) {
	NSLog(@"%@ -- 2",self.markupKey);
	[DrawPoint drawPointWithCGPoint:[[touches anyObject] locationInView:self]];
	[self setNeedsDisplay];
		
	if (_points == nil)
		_points = [[NSMutableArray alloc] init];
	
	[_points removeAllObjects];
	[_points addObject:[DrawPoint drawPointWithCGPoint:[[touches anyObject] locationInView:self]]];
	_drawing = YES;
	
	[self setNeedsDisplay];
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
//	NSLog(@"%@",touchesArray);
	if (_drawing)
	{
		if (self.editMode == YES) {
		[_points addObject:[DrawPoint drawPointWithCGPoint:[[touches anyObject] locationInView:self]]];
		[self setNeedsDisplay];
		}
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//	NSLog(@"%@",touchesArray);
	if (_drawing)
	{
			_drawing = NO;
		[self performRenderingToBackBuffer];
		[self setNeedsDisplay];
		}
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
}

- (BOOL)canBecomeFirstResponder
{
	return YES;
}

@end
