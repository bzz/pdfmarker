//
//  MenuViewController.h
//  PDFMarker
//
//  Created by Mikhail Baynov on 10/29/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuCell.h"

@interface MenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) UITableView *fileListTableView;
@property (strong, nonatomic) NSMutableArray *fileList;
@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) UITextField *textField;

@end
